"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const xmlControllers_1 = require("../controllers/xmlControllers");
class XmlRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        //this.router.get('/', (req, res) => res.send('archivo xlm'));
        this.router.post('/', xmlControllers_1.xmlController.xml);
    }
}
const xmlRoutes = new XmlRoutes();
exports.default = xmlRoutes.router;
