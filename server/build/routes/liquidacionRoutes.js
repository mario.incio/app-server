"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const liquidacionControllers_1 = __importDefault(require("../controllers/liquidacionControllers"));
class LiquidacionRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/', liquidacionControllers_1.default.index);
        this.router.post('/', liquidacionControllers_1.default.create);
        this.router.delete('/:id', liquidacionControllers_1.default.delete);
        this.router.put('/:id', liquidacionControllers_1.default.update);
    }
}
const liquidacionRoutes = new LiquidacionRoutes();
exports.default = liquidacionRoutes.router;
