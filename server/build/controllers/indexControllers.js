"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class IndexController {
    index(req, res) {
        //res.send('Hello');
        res.json({ id: 1, nombre: 'Mario', desc: 'Inicio del servicio' });
    }
}
exports.indexController = new IndexController();
