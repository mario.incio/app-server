"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const xmlString = `<?xml version="1.0" encoding="UTF-8"?>
<TestScenario>
   <TestSuite name="TS_EdgeHome">
      <TestCaseName name="tc_Login">dt_EdgeCaseHome,dt_EdgeCaseRoute</TestCaseName>
      <TestCaseName name="tc_Logout">dt_EdgeCaseRoute</TestCaseName>
   </TestSuite>
   <TestSuite name="TS_EdgePanel">
      <TestCaseName name="tc_AddContract">dt_EdgeCaseHome,dt_EdgeCaseSpectrum</TestCaseName>
   </TestSuite>
      <TestSuite name="TS_EdgeRoute">
      <TestCaseName name="tc_VerifyContract">dt_EdgeCaseRoute</TestCaseName>
      <TestCaseName name="tc_Payment">dt_EdgeCaseRoute</TestCaseName>
   </TestSuite>
   <TestSuite name="TS_EdgeSpectrum">
      <TestCaseName name="tc_ClientFeedback">dt_EdgeCaseSpectrum</TestCaseName>
   </TestSuite>
</TestScenario>`;
class XmlController {
    constructor() {
        this.xmls = '';
    }
    xml(req, res) {
        let JsonXml = req.body;
        let accountingsupplierparty = JsonXml.invoice["cac:accountingsupplierparty"]["cac:party"];
        res.json({
            "R.U.C": JsonXml.invoice["cac:signature"]["cac:signatoryparty"]["cac:partyidentification"]["cbc:id"],
            "Razón Social": accountingsupplierparty["cac:partyname"]["cbc:name"],
            //"Dirección": accountingsupplierparty["cac:partylegalentity"]["cac:registrationaddress"]["cac:addressline"]["cbc:line"],
            "Nro Documento": JsonXml.invoice["cbc:id"],
            "Fecha": JsonXml.invoice["cbc:issuedate"],
            //"SubTotal": JsonXml.invoice["cac:taxtotal"]["cac:taxsubtotal"]["cbc:taxableamount"]["_"],
            //"Impuesto": JsonXml.invoice["cac:taxtotal"]["cac:taxsubtotal"]["cbc:taxamount"]["_"],
            "Total": JsonXml.invoice["cac:legalmonetarytotal"]["cbc:payableamount"]["_"] //,
            //'JSON': JsonXml.invoice["cac:accountingsupplierparty"]["cac:party"]
        });
        /**valor.text().then((xml: any) =>{
            let parser = new DOMParser();
            let xmlDOM = parser.parseFromString(req.body, 'application/xml');
            let book = xmlDOM.querySelectorAll('book');

            book.forEach(bookXmlNode => {
                console.log(bookXmlNode.children);
            });
        });*/
    }
    xml2() {
        let xmlContent = '';
        fetch('XML factura.xml').then((res) => {
            res.text().then((xml => {
                xmlContent = xml;
                let parser = new DOMParser();
                let xmlDOM = parser.parseFromString(xmlContent, 'application/xml');
                let book = xmlDOM.querySelectorAll('book');
                book.forEach(bookXmlNode => {
                    console.log(bookXmlNode.children);
                });
            }));
        }).catch(err => {
            console.log(err);
        });
    }
}
exports.xmlController = new XmlController();
