"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = __importDefault(require("../database"));
class LiquidacionController {
    /**
     * index
req: Request, res: Response     */
    index(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            //let id = req.params.id;
            let id = 2;
            let query = "call GetAllUsers('" + id + "');";
            //let query = "select * from users";
            console.log(query);
            yield database_1.default.query(query, function (err, recordset) {
                if (err) {
                    console.log(err);
                }
                res.json(recordset);
            });
        });
    }
    create(req, res) {
        res.json({ text: 'Creating a liquidación ' + req.body });
    }
    delete(req, res) {
        res.json({ text: 'deleting liquidación' });
    }
    update(req, res) {
        res.json({ text: 'updating liquidación ' + req.params.id });
    }
}
const liquidacionController = new LiquidacionController();
exports.default = liquidacionController;
