"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mysql_1 = __importDefault(require("mysql"));
const configDB_1 = __importDefault(require("./configDB"));
const pool = mysql_1.default.createPool(configDB_1.default.database);
pool.getConnection(function (err, connection) {
    if (err)
        throw err; // not connected
    connection.release();
    console.log('conectado');
});
exports.default = pool;
