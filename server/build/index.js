"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const indexRoutes_1 = __importDefault(require("./routes/indexRoutes"));
const liquidacionRoutes_1 = __importDefault(require("./routes/liquidacionRoutes"));
const xmlRoutes_1 = __importDefault(require("./routes/xmlRoutes"));
const bodyParser = require('body-parser');
require('body-parser-xml')(bodyParser);
class Server {
    constructor() {
        this.app = express_1.default();
        this.config();
        this.routes();
    }
    config() {
        this.app.set('port', process.env.port || 3000);
        this.app.use(express_1.default.json());
        this.app.use(bodyParser.xml({
            limit: '1MB',
            xmlParseOptions: {
                normalize: true,
                normalizeTags: true,
                explicitArray: false // Only put nodes in array if >1
            }
        }));
    }
    routes() {
        this.app.use('/', indexRoutes_1.default);
        this.app.use('/api/liquidacion/', liquidacionRoutes_1.default);
        this.app.use('/api/xml/', xmlRoutes_1.default);
    }
    start() {
        this.app.listen(this.app.get('port'));
        console.log('server', this.app.get('port'));
    }
}
const server = new Server();
server.start();
