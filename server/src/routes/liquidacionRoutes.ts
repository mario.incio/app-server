import { Router } from "express";
import  liquidacionControllers  from "../controllers/liquidacionControllers";

class LiquidacionRoutes {
    router: Router = Router();

    constructor() {
        this.config();
    }

    config(): void {
        this.router.get('/', liquidacionControllers.index);
        this.router.post('/', liquidacionControllers.create);
        this.router.delete('/:id', liquidacionControllers.delete);
        this.router.put('/:id', liquidacionControllers.update);
    }
}

const liquidacionRoutes = new LiquidacionRoutes();

export default liquidacionRoutes.router;