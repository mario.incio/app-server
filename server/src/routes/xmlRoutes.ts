import { Router } from "express";
import { xmlController } from "../controllers/xmlControllers";

class XmlRoutes {
    router: Router = Router();

    constructor() {
        this.config();
    }

    config(): void {
        //this.router.get('/', (req, res) => res.send('archivo xlm'));
        this.router.post('/', xmlController.xml);
    }
}

const xmlRoutes = new XmlRoutes();

export default xmlRoutes.router;