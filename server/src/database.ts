import mysql from "mysql";

import  configDB  from "./configDB";

 const pool = mysql.createPool(configDB.database);
pool.getConnection(function(err, connection){
    if (err) throw err; // not connected

    connection.release();
    console.log('conectado')
})

 export default pool;