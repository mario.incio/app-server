import { Request, Response } from "express";
import pool from "../database";

class LiquidacionController {
    /**
     * index
req: Request, res: Response     */
    public async index(req: Request, res: Response): Promise<void> {
        //let id = req.params.id;
        let id = 2;
        let query = "call GetAllUsers('" + id + "');";
        //let query = "select * from users";
        console.log(query);
        await pool.query(query, function(err, recordset){
            if(err) {
                console.log(err);
            }
            res.json(recordset);
        });
    }

    public create (req:Request, res: Response) {
        res.json({text: 'Creating a liquidación ' + req.body});
    }

    public delete (req: Request, res: Response) {
        res.json({text: 'deleting liquidación'});
    }

    public update (req: Request, res: Response) {
        res.json({text: 'updating liquidación ' + req.params.id});
    }
}

const liquidacionController = new LiquidacionController();
export default liquidacionController;