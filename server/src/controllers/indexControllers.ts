import { Request, Response } from "express";

class IndexController {
    public index (req: Request, res: Response) {
        //res.send('Hello');
        res.json({id: 1, nombre: 'Mario', desc: 'Inicio del servicio'});
    }
}

export const indexController = new IndexController();