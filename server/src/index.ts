import  express,  { Application } from "express";
import  indexRoutes  from "./routes/indexRoutes";
import  liquidacionRoutes  from "./routes/liquidacionRoutes";
import  xmlRoutes from "./routes/xmlRoutes";

const bodyParser = require('body-parser');
require('body-parser-xml')(bodyParser);

class Server{

    public app: Application;

    constructor() {
       this.app = express();
       this.config();
       this.routes();
    }

    config():void {
        this.app.set('port', process.env.port || 3000); 
        this.app.use(express.json());
        this.app.use(bodyParser.xml({
            limit: '1MB',   // Reject payload bigger than 1 MB
            xmlParseOptions: {
              normalize: true,     // Trim whitespace inside text nodes
              normalizeTags: true, // Transform tags to lowercase
              explicitArray: false // Only put nodes in array if >1
            }
          }));
    }

    routes(): void {
        this.app.use('/', indexRoutes);
        this.app.use('/api/liquidacion/', liquidacionRoutes);
        this.app.use('/api/xml/', xmlRoutes);
    }

    start(): void {
        this.app.listen(this.app.get('port'));
        console.log('server', this.app.get('port'));
    }
}

const server = new Server();
server.start();